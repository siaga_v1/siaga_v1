-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2022 at 07:42 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siaga_v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `building`
--

CREATE TABLE `building` (
  `building_id` int(8) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `building_scanner` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `building`
--

INSERT INTO `building` (`building_id`, `code`, `name`, `address`, `building_scanner`) VALUES
(0, 'SWH6O/2022', 'Kantor Pusat', 'Jl. Trias Estate Blok C1/21 RT.002 RW.021 Cibitung', '');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `employees_code` varchar(20) NOT NULL,
  `employees_email` varchar(30) NOT NULL,
  `employees_password` varchar(100) NOT NULL,
  `employees_name` varchar(50) NOT NULL,
  `position_id` int(5) NOT NULL,
  `id_status` varchar(15) NOT NULL,
  `jht` varchar(15) NOT NULL,
  `jkm` varchar(15) NOT NULL,
  `jp` varchar(15) NOT NULL,
  `jkk` varchar(20) NOT NULL,
  `jk` varchar(15) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `created_login` datetime NOT NULL,
  `created_cookies` varchar(70) NOT NULL,
  `salary_month` int(10) NOT NULL,
  `allowance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `employees_code`, `employees_email`, `employees_password`, `employees_name`, `position_id`, `id_status`, `jht`, `jkm`, `jp`, `jkk`, `jk`, `shift_id`, `building_id`, `photo`, `created_login`, `created_cookies`, `salary_month`, `allowance`) VALUES
(1, 'Mis-001', 'testboe@test.com', '4d57bfe88f11ba8dd2c7f00eda77c635dd82e6dab629700916f25bc35babbacf', 'Aboedi', 7, 'TK/0', 'Aktif', 'Aktif', 'Aktif', 'Sangat Rendah', 'Aktif', 5, 0, '2022-11-2512af8aa1a037afe45044e3ccf63fc4d6.jpg', '2022-11-14 12:30:25', '16991bd286441c6f0a8fcfd50acb2804', 50000000, 200000),
(2, 'Mis-002', 'gen@test.com', '5b6330e11f83de43d029fd9a822d91c8e94f561785951548fb11351a435ff612', 'Gen', 2, 'TK/1', 'Aktif', 'Tidak Aktif', 'Tidak Aktif', 'Rendah', 'Aktif', 5, 0, '2022-11-06792ac0f046147073fd3041280c16df68.jpg', '2022-11-17 08:26:42', '16991bd286441c6f0a8fcfd50acb2804', 3000000, 200000),
(3, 'Mis-003', 'boe@test.com', '5b6330e11f83de43d029fd9a822d91c8e94f561785951548fb11351a435ff612', 'Ananda Budi Widyantoro', 1, 'TK/2', 'Tidak AKtif', 'Aktif', 'Tidak AKtif', 'Sedang', 'Tidak Aktif', 4, 0, '2022-11-177394b9f3a6476b834def869488873978.jpg', '2022-11-17 10:40:00', '-', 0, 0),
(4, 'Mis-004', 'gen@test.com', 'b3b838a3a37cb991fbd9b998b51603376c6ebfb83804385ee594df087db98edd', 'Gendhis', 8, 'TK/3', 'Tidak Aktif', 'Tidak AKtif', 'Aktif', 'Tinggi', 'Tidak Aktif', 5, 0, '2022-11-17599bfcc3483131d3f1c3569cf32c4c3a.jpg', '2022-11-17 10:50:00', '-', 0, 0),
(24, 'Mis-005', 'gen@test.com', 'b3b838a3a37cb991fbd9b998b51603376c6ebfb83804385ee594df087db98edd', 'ananda.budi', 7, 'K/1', 'Tidak AKtif', 'Tidak AKtif', 'Tidak AKtif', 'Sangat Tinggi', 'Aktif', 5, 0, '2022-11-24e95dfb9db7b2d9f0404d7087039858e3.jpeg', '2022-11-24 08:32:00', '-', 0, 0),
(25, 'Mis-006', 'gen@test.com', 'b3b838a3a37cb991fbd9b998b51603376c6ebfb83804385ee594df087db98edd', 'Gendhis', 1, 'K/2', 'Tidak AKtif', 'Tidak AKtif', 'Tidak AKtif', 'Tidak Aktif', 'Tidak Aktif', 4, 0, '2022-11-24fdf5d043ef42c055fe2712ea011e069f.jpg', '2022-11-24 08:34:00', '-', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE `holiday` (
  `holiday_id` int(5) NOT NULL,
  `holiday_name` varchar(25) NOT NULL,
  `holiday_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`holiday_id`, `holiday_name`, `holiday_date`) VALUES
(2, 'Tahun Baru 2023', '2023-01-01'),
(4, 'Hari Kemerdekaan', '2023-08-17');

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `position_id` int(5) NOT NULL,
  `position_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`position_id`, `position_name`) VALUES
(1, 'STAFF'),
(2, 'ACCOUNTING'),
(7, 'MANAGER'),
(8, 'DIREKSI');

-- --------------------------------------------------------

--
-- Table structure for table `presence`
--

CREATE TABLE `presence` (
  `presence_id` int(11) NOT NULL,
  `employees_id` int(11) NOT NULL,
  `presence_date` date NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL,
  `picture_in` varchar(150) NOT NULL,
  `picture_out` varchar(150) NOT NULL,
  `present_id` int(11) NOT NULL COMMENT 'Masuk,Pulang,Tidak Hadir',
  `presence_address` text NOT NULL,
  `information` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `presence`
--

INSERT INTO `presence` (`presence_id`, `employees_id`, `presence_date`, `time_in`, `time_out`, `picture_in`, `picture_out`, `present_id`, `presence_address`, `information`) VALUES
(26, 12, '2021-06-26', '05:05:00', '11:05:00', 'sari-murni-in-2021-06-26-12.jpg', 'sari-murni-out-2021-06-26-12.jpg', 1, '-1.4851831,102.43805809999999', ''),
(27, 12, '2021-06-30', '23:37:00', '00:00:00', 'sari-murni-in-2021-06-30-12.jpg', '', 1, '-1.4851831,102.43805809999999', ''),
(28, 18, '2021-06-30', '23:42:00', '00:00:00', 'juki--in-2021-06-30-18.jpg', '', 1, '-1.4851831,102.43805809999999', ''),
(29, 13, '2022-01-12', '14:31:00', '00:00:00', 'sari-ayu-in-2022-01-12-13.jpg', '', 1, '-1.6101229,103.6131203', ''),
(31, 17, '2022-10-31', '12:43:00', '00:00:00', 'ananda-budi-widyantoro-in-2022-10-31-17.jpg', '', 1, '-6.2513023,107.0808226', ''),
(32, 2, '2022-11-14', '07:22:23', '17:22:23', '', '', 1, '-6.2513023,107.0808226', ''),
(33, 2, '2022-11-14', '06:30:00', '17:31:00', 'gen-in-2022-11-14-2.jpg', 'gen-out-2022-11-14-2.jpg', 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `present_status`
--

CREATE TABLE `present_status` (
  `present_id` int(6) NOT NULL,
  `present_name` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `present_status`
--

INSERT INTO `present_status` (`present_id`, `present_name`) VALUES
(1, 'Hadir'),
(2, 'Sakit'),
(3, 'Izin');

-- --------------------------------------------------------

--
-- Table structure for table `salary`
--

CREATE TABLE `salary` (
  `employees_code` varchar(20) NOT NULL,
  `employees_name` varchar(50) NOT NULL,
  `salary_month` int(10) NOT NULL,
  `allowance` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `salary`
--

INSERT INTO `salary` (`employees_code`, `employees_name`, `salary_month`, `allowance`) VALUES
('Mis-001', 'Ananda Budi Widyantoro', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `shift_id` int(11) NOT NULL,
  `shift_name` varchar(20) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`shift_id`, `shift_name`, `time_in`, `time_out`) VALUES
(3, 'SHIFT SIANG', '13:00:00', '17:00:00'),
(4, 'SHIFT PAGI', '07:00:00', '11:00:00'),
(5, 'FULL TIME', '07:00:00', '17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `site_id` int(4) NOT NULL,
  `site_url` varchar(100) NOT NULL,
  `site_name` varchar(50) NOT NULL,
  `site_phone` char(12) NOT NULL,
  `site_address` text NOT NULL,
  `site_description` text NOT NULL,
  `site_logo` varchar(50) NOT NULL,
  `site_email` varchar(30) NOT NULL,
  `site_email_domain` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`site_id`, `site_url`, `site_name`, `site_phone`, `site_address`, `site_description`, `site_logo`, `site_email`, `site_email_domain`) VALUES
(1, 'http://localhost/siaga_v1', 'Presensi & Payroll Karyawani', '081298859865', 'Jl. Ceri 6 Blok C/21 RT.02 RW.021 Wanasari, Cibitung. Kab. Bekasi', 'Aplikasi Sistem Absensi Online Berbasis Foto Selfie dan Auto Detect Lokasi. Absen Karyawan Kini Jadi Lebih Efisien. Sistem absensi dengan verifikasi foto selfie atau webcam, dilengkapi fitur deteksi lokasi pengguna yang akurat.', 'pngitem_3769041png.png', 'ananda.budiw@gmail.com', 'ananda.budiw@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id_status` varchar(15) NOT NULL,
  `status_name` varchar(15) NOT NULL,
  `child` int(3) NOT NULL,
  `minimum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id_status`, `status_name`, `child`, `minimum`) VALUES
('K/0', 'Menikah', 0, 58500000),
('K/1', 'Menikah', 1, 63000000),
('K/2', 'Menikah', 2, 67500000),
('K/3', 'Menikah', 3, 72000000),
('TK/0', 'Single', 0, 54000000),
('TK/1', 'Single', 1, 58500000),
('TK/2', 'Single', 2, 63000000),
('TK/3', 'Single', 3, 67500000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(40) NOT NULL,
  `registered` datetime NOT NULL,
  `created_login` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `session` varchar(100) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `browser` varchar(30) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `email`, `password`, `fullname`, `registered`, `created_login`, `last_login`, `session`, `ip`, `browser`, `level`) VALUES
(1, 'admin', 'coding@test.com', '88b3340abaa6acbf87abe45f68fa8960224c1e36f6a96433bcbc490c84c9c6d2', 'ADMIN', '2021-02-03 10:22:00', '2022-11-28 11:45:35', '2022-11-06 07:32:40', '-', '1', 'Google Crome', 1),
(4, 'Kaspulmin', 'kaspul@test.com', '1109908074cff88e53cd8b6cd385350408caef5aa7527fc1e59ccd3ba8120576', 'Kaspul de Caprio', '2022-11-03 20:21:00', '2022-11-03 20:21:00', '2022-11-03 20:21:00', '0', '1', 'Google Crome', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_level`
--

CREATE TABLE `user_level` (
  `level_id` int(4) NOT NULL,
  `level_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_level`
--

INSERT INTO `user_level` (`level_id`, `level_name`) VALUES
(1, 'Administrator'),
(2, 'Operator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `building`
--
ALTER TABLE `building`
  ADD PRIMARY KEY (`building_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`holiday_id`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`position_id`);

--
-- Indexes for table `presence`
--
ALTER TABLE `presence`
  ADD PRIMARY KEY (`presence_id`);

--
-- Indexes for table `present_status`
--
ALTER TABLE `present_status`
  ADD PRIMARY KEY (`present_id`);

--
-- Indexes for table `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`employees_code`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`shift_id`);

--
-- Indexes for table `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`site_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`level_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `building`
--
ALTER TABLE `building`
  MODIFY `building_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `holiday`
--
ALTER TABLE `holiday`
  MODIFY `holiday_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `position_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `presence`
--
ALTER TABLE `presence`
  MODIFY `presence_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `present_status`
--
ALTER TABLE `present_status`
  MODIFY `present_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `shift_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `site`
--
ALTER TABLE `site`
  MODIFY `site_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_level`
--
ALTER TABLE `user_level`
  MODIFY `level_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
