<?php 
session_start();
if(empty($connection)){
  header('location:../../');
} else {
  include_once 'mod/sw-panel.php';
echo'
  <div class="content-wrapper">';
    switch(@$_GET['op']){ 
    default:
echo'
<section class="content-header">
  <h1>Data<small> Gaji</small></h1>
    <ol class="breadcrumb">
      <li><a href="./"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Data Gaji</li>
    </ol>
</section>';
echo'
<section class="content">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><b>Data Gaji</b></h3>
        </div>
<div class="box-body">
<table id="swdatatable" class="table table-bordered">
  <thead>
  <tr>
    <th style="width:10px">No</th>
    <th>Kode</th>
    <th>Nama Karyawan</th>
    <th>Gaji</th>
    <th>Tunjangan</th>
    <th>Total</>
    <th style="width:40px" class="text-right">Aksi</th>

  </tr>
  </thead>
  <tbody>';


  $query="SELECT employees_code,employees_name,salary_month,allowance FROM employees order by employees_code ASC";
  $result = $connection->query($query);
  if($result->num_rows > 0){
  $no=0;
 while ($row= $result->fetch_assoc()) {
    $no++;
    echo'
      <td class="text-center">'.$no.'</td>
      <td>'.$row['employees_code'].'</td>
      <td>'.$row['employees_name'].'</td>
      <td>Rp.'.number_format($row['salary_month'],0,',','.').'</td>
      <td>Rp.'.number_format($row['allowance'],0,',','.').'</td>
      <td></td>
      <td>
        <div class="btn-group">';
        if($level_user==1){
          echo'
          <a href="#modalEdit" class="btn btn-warning btn-xs enable-tooltip" title="Edit" data-toggle="modal"';?> onclick="getElementById('txtcode').value='<?PHP echo $row['employees_code'];?>';getElementById('txtnama').value='<?PHP echo $row['employees_name'];?>';getElementById('txtgaji').value='<?PHP echo $row['salary_month'];?>';getElementById('txttunjangan').value='<?PHP echo $row['allowance'];?>';"><i class="fa fa-pencil-square-o"></i> UBAH</a>
      <?php echo'
      ';}
      else {
        echo'
          <button type="button" class="btn btn-warning btn-xs access-failed enable-tooltip" title="Edit"><i class="fa fa-pencil-square-o"></i> UBAH</button>';
      }echo'
        </div>
      </td>
    </tr>';}}
  echo'
  </tbody>
</table>
      </div>
    </div>
  </div> 
</section>

<!-- MODAL EDIT -->
<div class="modal fade" id="modalEdit" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update Data</h4>
      </div>
      
    <form class="form update-gaji" method="post" name="formgaji">
      <div class="modal-body">

          <div class="form-group">
              <label>Kode Karyawan</label>
              <input type="text" class="form-control" name="employees_code" id="txtcode" readonly required>
          </div>

          <div class="form-group">
              <label>Nama Karyawan</label>
              <input type="text" class="form-control" name="employees_name" id="txtnama" readonly required>
          </div>

          <div class="form-group">
            <label>Gaji Karyawan</label>
            <input type="text" class="form-control" name="salary_month" id="txtgaji" required >
          </div>

          <div class="form-group">
            <label>Tunjangan </label>
            <input type="text" class="form-control" name="allowance" id="txttunjangan" onkeyup="OnChange(this.value)" onkeypress="return IsNumberKey(event)" required>
          </div>

          <div class="form-group">
            <label>Total </label>
            <input type="text" class="form-control" name="total" id="txttotal" onkeyup="OnChange(this.value)" onkeypress="return IsNumberKey(event)" required>
          </div>

      </div>

      <div class="modal-footer">
        <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-check"></i> Simpan</button>
        <button type="button" class="btn btn-danger pull-right" data-dismiss="modal"><i class="fa fa-remove"></i> Batal</button>
      </div>
    </form>

    <script type="text/javascript" language="Javascript">
   totalgaji = document.formgaji.salary_month.value;
   document.formgaji.total.value = totalgaji;
   subgaji = document.formgaji.allowance.value;
   document.formgaji.total.value = subgaji;
   function OnChange(value){
     totalgaji = document.formgaji.salary_month.value;
     subgaji = document.formgaji.allowance.value  ;
     total = parseInt(totalgaji) + parseInt(subgaji);
     document.formgaji.total.value = total;
   }
    </script>

    </div>
  </div>
</div>';
break;
}?>
</div>
<?php }?> 


